// Copyright 2019 SMS
// License(GPL)
// Author: ShenMian

#include <winsock2.h>
#include "type.h"

#ifndef SOCKET_H_
#define SOCKET_H_

class Socket
{
public:
	Socket();
	~Socket();

	void	 socket(const SOCKET sock);
	SOCKET socket() const;

	void	 ip(const string& ip);
	string ip() const;

	void	 port(const ushort);
	ushort port() const;

	bool send(string& buf);
	bool recv(string& buf);

	void send_file(string& path);
	void recv_file(string& path);

	bool connect();
	bool connect(string& ip, ushort port);

	bool bind(ushort port);
	bool bind(string& ip, ushort port);

	bool listen();
	bool listen(ushort logback);

	bool accept(Socket& sock);

	ushort error();

private:
	SOCKET			sock_;
	sockaddr_in addr_;

	ushort			error_;	// 最后错误代码
	static uint size_;	// 实例总数
};

#endif	// SOCKET_H_
