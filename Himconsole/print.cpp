// Copyright 2019 SMS
// License(GPL)
// Author: ShenMian

#include "print.h"
#include <conio.h>
#include <stdio.h>

namespace print
{
void info(const string str)
{
	color::fore::blue();
	color::mode::bold();
	printf("[*] ");
	color::rest();
	puts(str.c_str());
}

void good(const string str)
{
	color::fore::green();
	color::mode::bold();
	printf("[+] ");
	color::rest();
	puts(str.c_str());
}

void err(const string str)
{
	color::fore::red();
	color::mode::bold();
	printf("[-] ");
	color::rest();
	puts(str.c_str());
}

void warn(const string str)
{
	color::fore::yellow();
	color::mode::bold();
	printf("[!] ");
	color::rest();
	puts(str.c_str());
}



namespace list
{

void title(ushort num, ...)
{
	void* pArg = &num + sizeof(ushort);

	printf("[%hu][%s]", num, pArg);
}

void line(...)
{

}

ushort space[];

}	// namespace list



#include <windows.h>
void excp(const char* file, const char* func, int line)
{
	color::fore::red();
	color::mode::bold();
	printf(
			"\n"
			"[Exception]\n"
			"FILE: %s\n"
			"FUNC: %s\n"
			"LINE: %d\n"
			"CODE: %d\n"
			"�� ����� ����...",
			file, func, line, GetLastError());
	_getch();
	_putch('\n');
	color::rest();
}

}	// namespace print
