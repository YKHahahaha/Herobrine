**[简体中文](README_zh-CN.md)|繁體中文|[EN](README_en.md)**

![橫幅](banner_cn.jpg)  

**提示** 目前公布的源代碼為早期編寫的半成品, 最新的源代碼將盡快上傳.  

**子項目**  
- [**Socket**](https://gitee.com/ShenMian/Socket): 小型網絡通信C++庫  

**特點**(部分還在實現中)  
- "雲"命令  
  執行命令時會先下載並載入相應的模塊, 並在合適的時候卸載. 有效減小被控端的體積.  
  [**基本命令**](https://gitee.com/ShenMian/Herobrine/wikis/%E5%9F%BA%E6%9C%AC%E5%91%BD%E4%BB%A4)  
- 繞過查殺軟件  
  利用的被控端中的遠程執行漏洞來植入HIM, 有效繞過基於特征/線索/模擬運行環境的查殺技術. 使HIM直接在內存中運行而殺毒軟件和用戶渾然不覺.  
- 跳躍Web認證上網  
  許多公共場所會普及Web認證上網, HIM將利用同個局域網中的HIM作為跳板, 與外網連接, 以此繞過認證.  
- 人機交互友好  
  命令行具有輸入預測提示/Tab壹鍵補全/上下鍵切換歷史命令等功能.  
	[**該功能測試版下載**](https://gitee.com/ShenMian/Herobrine/releases/v0.0.0001)  
- 動態更新主控端IP地址  
  從給定的URL列表指定的頁面中自動獲取最新的主控端IP地址.  
- 支持數據庫  
  可以利用MySQL數據庫存儲數據, 使團隊協作變得輕松簡單, 無需擔心本地同步等問題.  
- 繞過防火墻  
  - 通過多種常見應用層協議封裝數據來繞過防火墻.  
  - 采用TCP反彈式連接以繞過防火墻.  
- 代碼風格嚴謹  
  嚴格按照壹定規範進行編寫, 使源代碼具有較高的可讀性.  
- 永久開放源代碼  
  Herobrine已宣布永久開放源代碼, 目前遵循Apache-2.0開源協議.  

詳情請翻閱[**Wiki**](https://gitee.com/ShenMian/Herobrine/wikis/).  

**反饋**  
歡迎提交錯誤或建議到[**Issues**](https://gitee.com/ShenMian/Herobrine/issues), 或發送電子郵件至`shenmian000@outlook.com`/`shenmian000@foxmail.com`.  

**免責聲明**  
在未經事先雙方同意的情況下使用Herobrine攻擊目標是非法的. 最終用戶有責任承擔所有法律責任. 開發人員不承擔任何責任, 也不對此計劃造成的任何誤用或損害負責.  

**早期版本**效果圖  
![早期版本效果圖](old.jpg)
