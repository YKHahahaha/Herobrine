**[简体中文](README_zh-CN.md)|[繁體中文](README_zh-TW.md)|EN**  

![横幅](banner_cn.jpg)  

For more information, please refer to [**Wiki**](https://gitee.com/ShenMian/Herobrine/wikis/).  

 **Feedback**  
Welcome to submit errors or suggestions to [Issues](https://gitee.com/ShenMian/Herobrine/issues), or send e-mail to `shenmian000@outlook.com`/`shenmian000@foxmail.com`.  

 **Disclaimer**  
Usage of herobrine for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program.  
